using System.ComponentModel.DataAnnotations;
using Company.Pages;

namespace Company.Models
{
    public class Assignment
    {
        [Key]
        public int Id {get;set;}
        
        public int EmployeeId {get;set;}
        public int TaskId {get;set;}
        public Employee Employee {get;set;}
        public Objective Objective {get;set;}
    }
}