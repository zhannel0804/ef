using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Company.Models;

namespace Company.Pages
{
    public class Employee
    {
        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Column("FirstName")]
        [Display(Name = "First Name")]
        public string FirstName {get;set;}
        [Required]
        [StringLength(50)]
        [Display(Name = "Last Name")]
        public string LastName {get;set;}
        
        //Navigazia
        public ICollection<Assignment> Assignments {get;} = new List <Assignment>();

        //Foreign key
        public int EmployeeId {get;set;}
        public int OrgUnitForeignKey {get;set;}
        
        [ForeignKey("OrgUnitForeignKey")]
        public OrgUnit OrgUnit {get;set;}
    }
}