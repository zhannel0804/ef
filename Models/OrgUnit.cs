using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Company.Pages
{
    public class OrgUnit
    {
        public int Id {get;set;}
        public OrgUnit Parent {get;set;}
        public int ParentId {get;set;}

        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Column("Name")]
        [Display(Name = "Name")]
        public string Name {get;set;}

        //Navigazia
        public virtual ICollection<OrgUnit> SubOrgUnits {get;set;}
        public virtual ICollection<Employee> Employees {get;set;}
    }
}