using System;
using System.Linq;
using Company.Pages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;

namespace Company.Models
{
    public static class SeedData
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {
            using (var context = new CompanyContext(
                serviceProvider.GetRequiredService<DbContextOptions<CompanyContext>>()))
            {
                if (context.OrgUnit.Any())
                {
                    return;
                }
                var orgUnits = new OrgUnit[]
                {
                    new OrgUnit {Name="HR", Id=1, 
                                SubOrgUnits=new List<OrgUnit>()
                                {
                                   new OrgUnit 
                                    {
                                        Name="Almaty", Id=2
                                    },
                                    new OrgUnit
                                    {
                                        Name="Astana", Id=3
                                    }
                                }},
                    new OrgUnit {Name="Administration", Id=4, 
                                SubOrgUnits=new List<OrgUnit>()
                                {
                                    new OrgUnit
                                    {
                                        Name="Almaty", Id=5
                                    },
                                    new OrgUnit
                                    {
                                        Name="Astana", Id=6
                                    }

                                }}
                    
                };
                foreach (OrgUnit o in orgUnits)
                    {
                        context.OrgUnit.Add(o);
                    }
                    context.SaveChanges();
                var employee = new Employee[]
                {
                    new Employee {FirstName="Zhannel", LastName="Ikhsanova-Solod",
                                    EmployeeId=1, OrgUnitForeignKey = 1
                        },
                    new Employee {FirstName="Akerke", LastName="Aitanova", 
                                    EmployeeId=2, OrgUnitForeignKey=4
                    }
                };
                foreach(Employee i in employee)
                {
                    context.Employee.Add(i);
                }
                context.SaveChanges();
                var objective = new Objective[]
                {
                    new Objective 
                    {
                        Name = "A", ObjectiveId=1
                    },
                    new Objective
                    {
                        Name = "B", ObjectiveId = 2
                    },
                    new Objective
                    {
                        Name = "C", ObjectiveId = 3
                    }
                };
                foreach(Objective n in objective)
                {
                    context.Objective.Add(n);
                }
                context.SaveChanges();
            }
        }
    }
}