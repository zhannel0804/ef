using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Company.Models;

namespace Company.Pages
{
    public class Objective
    {
        [Required]
        [StringLength(50, ErrorMessage = "First name cannot be longer than 50 characters.")]
        [Column("Name")]
        [Display(Name = "Name")]
        public string Name {get;set;}
        public int ObjectiveId {get;set;}

        
        //Navigazia
        public ICollection<Assignment> Assignments {get;} = new List <Assignment>();
    }
}