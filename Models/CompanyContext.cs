using Company.Pages;
using Microsoft.EntityFrameworkCore;

namespace Company.Models
{
    public class CompanyContext: DbContext
    {
        public CompanyContext (DbContextOptions<CompanyContext> options)
            : base(options)
        {    
        }
        public DbSet<OrgUnit> OrgUnit { get; set; }
        public DbSet<Employee> Employee { get; set; }
        public DbSet<Objective> Objective { get; set; }
        public DbSet<Assignment> Assignment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Assignment>()
                .HasKey(c => new { c.EmployeeId, c.TaskId });
                
            modelBuilder.Entity<OrgUnit>()
                .Property(b => b.Id)
                .IsRequired();
        }
    }
}