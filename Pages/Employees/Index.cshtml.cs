using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.Employees
{
    public class IndexModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public IndexModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public IList<Employee> Employee { get;set; }

        public async Task OnGetAsync()
        {
            Employee = await _context.Employee
                .Include(e => e.OrgUnit).ToListAsync();
        }
    }
}
