using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.OrgUnits
{
    public class DetailsModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public DetailsModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public OrgUnit OrgUnit { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrgUnit = await _context.OrgUnit
                .Include(o => o.Parent).FirstOrDefaultAsync(m => m.Id == id);

            if (OrgUnit == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
