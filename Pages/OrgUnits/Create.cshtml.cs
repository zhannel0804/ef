using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Company.Models;
using Company.Pages;

namespace Company.Pages.OrgUnits
{
    public class CreateModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public CreateModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["ParentId"] = new SelectList(_context.OrgUnit, "Id", "Id");
            return Page();
        }

        [BindProperty]
        public OrgUnit OrgUnit { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.OrgUnit.Add(OrgUnit);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}