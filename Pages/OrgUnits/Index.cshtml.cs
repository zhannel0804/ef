using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.OrgUnits
{
    public class IndexModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public IndexModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public IList<OrgUnit> OrgUnit { get;set; }

        public async Task OnGetAsync()
        {
            OrgUnit = await _context.OrgUnit
                .Include(o => o.Parent).ToListAsync();
        }
    }
}
