using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.OrgUnits
{
    public class EditModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public EditModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public OrgUnit OrgUnit { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            OrgUnit = await _context.OrgUnit
                .Include(o => o.Parent).FirstOrDefaultAsync(m => m.Id == id);

            if (OrgUnit == null)
            {
                return NotFound();
            }
           ViewData["ParentId"] = new SelectList(_context.OrgUnit, "Id", "Id");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(OrgUnit).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrgUnitExists(OrgUnit.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool OrgUnitExists(int id)
        {
            return _context.OrgUnit.Any(e => e.Id == id);
        }
    }
}
