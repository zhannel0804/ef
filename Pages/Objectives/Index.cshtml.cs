using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.Objectives
{
    public class IndexModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public IndexModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public IList<Objective> Objective { get;set; }

        public async Task OnGetAsync()
        {
            Objective = await _context.Objective.ToListAsync();
        }
    }
}
