using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.Objectives
{
    public class DetailsModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public DetailsModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        public Objective Objective { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Objective = await _context.Objective.FirstOrDefaultAsync(m => m.ObjectiveId == id);

            if (Objective == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
