using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Company.Models;
using Company.Pages;

namespace Company.Pages.Objectives
{
    public class EditModel : PageModel
    {
        private readonly Company.Models.CompanyContext _context;

        public EditModel(Company.Models.CompanyContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Objective Objective { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Objective = await _context.Objective.FirstOrDefaultAsync(m => m.ObjectiveId == id);

            if (Objective == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Objective).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ObjectiveExists(Objective.ObjectiveId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ObjectiveExists(int id)
        {
            return _context.Objective.Any(e => e.ObjectiveId == id);
        }
    }
}
